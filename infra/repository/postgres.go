package repository

import (
	"context"
	"database/sql"
	"log"
	"time"
)

type postgres struct {
	db *sql.DB
}

func NewPostgres(db *sql.DB) repository {
	return postgres{db}
}

func (repo postgres) SavePrice(ctx context.Context, exchange string, price string, symbol string, t time.Time) error {
	q := `INSERT INTO prices (exchange, price, symbol, "time") VALUES($1, $2, $3, current_timestamp)`
	_, err := repo.db.ExecContext(ctx, q, exchange, price, symbol)
	return err
}

func (repo postgres) GetHistory(ctx context.Context, exchange, symbol string, startDate, endDate time.Time) (map[string]string, error) {
	q := `SELECT price, "time" from prices where symbol = $1 and exchange = $2 and "time" >= $3 and "time" <= $4`
	rows, err := repo.db.QueryContext(ctx, q, symbol, exchange, startDate, endDate)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	resp := map[string]string{}

	for rows.Next() {
		var name string
		var t string
		if err := rows.Scan(&name, &t); err != nil {
			log.Fatal(err)
		}

		resp[t] = name
	}

	return resp, nil
}
