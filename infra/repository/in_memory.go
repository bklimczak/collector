package repository

import (
	"context"
	"time"
)

type inMemory struct {
	history map[time.Time]string
}

type repository interface {
	SavePrice(ctx context.Context, exchange string, price string, symbol string, time time.Time) error
	GetHistory(ctx context.Context, exchange, symbol string, startDate, endDate time.Time) (map[string]string, error)
}

func NewInMemory() repository {
	return &inMemory{
		history: map[time.Time]string{},
	}
}

func (repo *inMemory) SavePrice(ctx context.Context, exchange string, price string, symbol string, t time.Time) error {
	repo.history[t] = price
	return nil
}

func (repo inMemory) GetHistory(ctx context.Context, exchange, symbol string, startDate, endDate time.Time) (map[string]string, error) {
	history := map[string]string{}

	for t, p := range repo.history {
		if inTimeRange(t, startDate, endDate) {
			history[t.UTC().Format("2006-01-02T15:04:05.999Z")] = p
		}
	}

	return history, nil
}

func inTimeRange(t, start, end time.Time) bool {
	return (t.After(start) && t.Before(end)) || t.Equal(start) || t.Equal(end)
}
