package fetcher

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/bklimczak/collector/pkg/collector"
)

const binanceUrl = "https://api.binance.com/api/v3/ticker/price"

type binance struct {
	client http.Client
}

func NewBinance(client http.Client) collector.Fetcher {
	return binance{client: client}
}

func (f binance) Exchange() string {
	return "binance"
}

func (f binance) Fetch(ctx context.Context) ([]collector.Price, error) {
	resp, err := f.client.Get(binanceUrl)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("failed fetching data from binance")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	prices := []collector.Price{}
	err = json.Unmarshal(body, &prices)
	if err != nil {
		return nil, err
	}

	for _, p := range prices {
		p.Time = time.Now().UTC()
	}

	return prices, nil
}
