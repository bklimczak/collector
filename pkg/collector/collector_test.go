package collector

import (
	"context"
	"net/http"
	"testing"
	"time"

	"bitbucket.org/bklimczak/collector/infra/repository"
	"github.com/stretchr/testify/assert"
)

func TestCollector(t *testing.T) {
	c := New(httpRegistrator{}, repository.NewInMemory(), nil)
	ctx := context.Background()
	prices := []Price{
		{
			Symbol: "XYZ",
			Price:  "1234",
			Time:   time.Now(),
		},
	}

	err := c.record(ctx, "XYZ", prices)
	assert.NoError(t, err)
}

func TestCollector_GetSummaryOfUpdates(t *testing.T) {
	c := New(httpRegistrator{}, repository.NewInMemory(), nil)
	ctx := context.Background()
	//"2020-02-10T00:00:00Z"
	startDate := time.Date(2020, 02, 10, 0, 0, 0, 0, time.UTC)
	// "2020-02-14T23:59:59Z"
	endDate := time.Date(2020, 02, 14, 23, 59, 59, 0, time.UTC)

	prices := []Price{
		{
			Symbol: "bitcoin",
			Price:  "10130.88",
			Time:   time.Date(2020, 02, 10, 0, 0, 0, 0, time.UTC),
		},
		{
			Symbol: "bitcoin",
			Price:  "10130.87",
			Time:   time.Date(2020, 02, 10, 0, 0, 1, 0, time.UTC),
		},
		{
			Symbol: "bitcoin",
			Price:  "10130.43",
			Time:   time.Date(2020, 02, 10, 0, 0, 2, 0, time.UTC),
		},
	}

	err := c.record(ctx, "bitcoin", prices)
	assert.NoError(t, err)

	updates, err := c.History(ctx, "binance", "bitcoin", startDate, endDate)
	assert.NoError(t, err)
	assert.ElementsMatch(t, []HistoryEntry{
		{
			Timestamp: "2020-02-10T00:00:00Z",
			Price:     "10130.88",
		},
		{
			Timestamp: "2020-02-10T00:00:01Z",
			Price:     "10130.87",
		},
		{
			Timestamp: "2020-02-10T00:00:02Z",
			Price:     "10130.43",
		},
	}, updates)
}

type httpRegistrator struct {
}

func (h httpRegistrator) Handle(pattern string, handler http.Handler) {
}
