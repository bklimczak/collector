package collector

import (
	"context"
	"log"
	"time"
)

type Price struct {
	Price  string    `json:"price"`
	Symbol string    `json:"symbol"`
	Time   time.Time `json:"time"`
}

type HistoryEntry struct {
	Price     string `json:"price"`
	Timestamp string `json:"timestamp"`
}

type Repository interface {
	SavePrice(ctx context.Context, exchange string, price string, symbol string, time time.Time) error
	GetHistory(ctx context.Context, exchange, symbol string, startDate, endDate time.Time) (map[string]string, error)
}

type Fetcher interface {
	Fetch(ctx context.Context) ([]Price, error)
	Exchange() string
}

type Collector struct {
	repo    Repository
	fetcher Fetcher
}

func (c Collector) Run(ctx context.Context) error {
	ticker := time.NewTicker(time.Second)

	for {
		select {
		case <-ctx.Done():
			return nil
		case <-ticker.C:
			go c.run()
		}
	}
}

func (c Collector) run() {
	ctx := context.Background()
	prices, err := c.fetcher.Fetch(ctx)
	if err != nil {
		log.Fatal(err)
		return
	}

	err = c.record(ctx, c.fetcher.Exchange(), prices)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func (c Collector) record(ctx context.Context, exchange string, prices []Price) error {
	for _, p := range prices {
		err := c.repo.SavePrice(ctx, exchange, p.Price, p.Symbol, p.Time)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c Collector) History(ctx context.Context, exchange, symbol string, startDate, endDate time.Time) ([]HistoryEntry, error) {
	// we can add some ordering here but I ignore it for now
	res, err := c.repo.GetHistory(ctx, exchange, symbol, startDate, endDate)
	if err != nil {
		return nil, err
	}

	updates := []HistoryEntry{}

	for timestamp, price := range res {
		updates = append(updates, HistoryEntry{
			Price:     price,
			Timestamp: timestamp,
		})
	}

	return updates, nil
}
