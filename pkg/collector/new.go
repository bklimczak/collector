package collector

import "net/http"

type Handler interface {
	Handle(pattern string, handler http.Handler)
}

func New(registrator Handler, repo Repository, fetcher Fetcher) Collector {
	c := Collector{repo: repo, fetcher: fetcher}
	handler := httpHandler{collector: c}
	registrator.Handle("/prices", handler)

	return c
}
