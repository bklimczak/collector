package collector

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

type httpHandler struct {
	collector Collector
}

type Request struct {
	Exchange string    `json:"exchange"`
	Symbol   string    `json:"symbol"`
	From     time.Time `json:"from"`
	To       time.Time `json:"to"`
}

func (h httpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request"))
		return
	}

	req := Request{}
	err = json.Unmarshal(body, &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("bad request"))
		return
	}

	history, err := h.collector.History(r.Context(), req.Exchange, req.Symbol, req.From, req.To)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server error"))
		return
	}

	body, err = json.Marshal(history)

	w.Write(body)
}
