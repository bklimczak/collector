package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/bklimczak/collector/infra/fetcher"
	"bitbucket.org/bklimczak/collector/infra/repository"
	"bitbucket.org/bklimczak/collector/pkg/collector"
	_ "github.com/lib/pq"
)

func main() {
	db := getDB()

	defer db.Close()

	client := http.Client{
		Timeout: time.Second,
	}

	f := fetcher.NewBinance(client)
	r := repository.NewPostgres(db)
	Run(f, r)
}

func Run(fetcher collector.Fetcher, repo collector.Repository) {
	mux := http.NewServeMux()
	c := collector.New(mux, repo, fetcher)
	ctx := context.Background()

	log.Print("starting...")
	go http.ListenAndServe(":8080", mux)
	c.Run(ctx)
}

func envOrPanic(name string) string {
	v := os.Getenv(name)
	if v == "" {
		panic(fmt.Sprintf("the env %s does not exist", name))
	}

	return v
}

func getDB() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		envOrPanic("POSTGRES_HOST"), envOrPanic("POSTGRES_USER"), envOrPanic("POSTGRES_PASSWORD"), envOrPanic("POSTGRES_DB"))

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}
	return db
}
