// +build !integration

package main_test

import (
	"os"
	"testing"

	app "bitbucket.org/bklimczak/collector"
	"bitbucket.org/bklimczak/collector/infra/fetcher"
	"bitbucket.org/bklimczak/collector/infra/repository"
)

func TestMain(m *testing.M) {
	repo = repository.NewInMemory()
	fetch = fetcher.NewBinance(client)
	go app.Run(fetch, repo)

	waitForServiceRunning()
	os.Exit(m.Run())
}
