package main_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"

	"bitbucket.org/bklimczak/collector/pkg/collector"
	"github.com/stretchr/testify/assert"
)

var fetch collector.Fetcher
var repo collector.Repository
var client http.Client

func TestShouldReturnBadRequest(t *testing.T) {
	httpReq, _ := http.NewRequest(http.MethodGet, url("prices"), nil)
	resp, err := client.Do(httpReq)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
}

func TestShouldReturnStatusOK(t *testing.T) {
	req := collector.Request{
		Exchange: "bitname",
		Symbol:   "XYZ",
	}

	body, _ := json.Marshal(req)

	httpReq, _ := http.NewRequest(http.MethodPost, url("prices"), bytes.NewBuffer(body))
	resp, err := client.Do(httpReq)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func url(path string) string {
	return fmt.Sprintf("http://localhost:%s/%s", "8080", path)
}

func serviceRunning() bool {
	resp, err := http.Get(url("prices"))
	return err == nil && resp.StatusCode == http.StatusBadRequest
}

func waitForServiceRunning() {
	healthy := Eventually(serviceRunning, time.Second*5, time.Millisecond*100)
	if !healthy {
		panic("Unable to connect to the server")
	}
}

func Eventually(functionToCheck func() bool, waitFor time.Duration, tick time.Duration) bool {
	timer := time.NewTimer(waitFor)
	ticker := time.NewTicker(tick)
	checkPassed := make(chan bool)
	defer timer.Stop()
	defer ticker.Stop()

	go func() {
		checkPassed <- functionToCheck()
	}()
	for {
		select {
		case <-timer.C:
			return false
		case result := <-checkPassed:
			if result {
				return true
			}
		case <-ticker.C:
			go func() {
				checkPassed <- functionToCheck()
			}()
		}
	}
}
