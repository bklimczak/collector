CREATE TABLE public.prices (
    id serial NOT NULL,
    exchange varchar NOT NULL,
    price varchar NOT NULL,
    symbol varchar NOT NULL,
    "time" timestamptz NOT NULL,
    CONSTRAINT prices_pk PRIMARY KEY (id)
);

