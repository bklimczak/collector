## basic info

Prices are available on `http://localhost:8080/prices`. It requires a request with body:

```json
{
    "exchange": "binance",
    "symbol": "btcusd",
    "from": "2020-02-10T00:00:00Z",
    "to": "2020-02-14T23:59:59Z"
}
```

## test it

```
make tests
```


## run it

```
make run
```
