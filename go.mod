module bitbucket.org/bklimczak/collector

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.3.0
	github.com/stretchr/testify v1.4.0
)
